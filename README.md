# Windows Terminal
Windows 10 Terminal settings file.
Additional profiles:
- iPython console
- git-bash console
- Debian WSL console

## Installation
### iPython
Use package manager [pip](https://pip.pypa.io/en/stable/) to install ipython
```python
pip install ipyhton
```

### git-bash
Use [git](https://git-scm.com/download/win) installer for windows to instal git-bash 64-bit.

### Debian WSL
Follow Microsoft documentation to install [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

### Windows Terminal personalization
Replace file located in:
```
%HOMEPATH%\AppData\Local\Packages\Microsoft.Windows.Terminal_xxxxxxxxxxxxx\LocalState\
```

## Usage
Start Windows Terminal

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html)
